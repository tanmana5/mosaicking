import numpy as np
import geopy
import geopy.distance


# find the nearest coordinate pair to given within list of all coordinates
def find_nearest(x, y,  keys):
    pts = [geopy.Point(p[0], p[1]) for p in keys]
    onept = geopy.Point(x, y)
    alldist = [(p, geopy.distance.distance(p, onept).km) for p in pts]
    nearest_point = min(alldist, nkey=lambda x: (x[1]))[0]
    return nearest_point


# dummy dictionary of coodinates as keys and images as values
dict_images = dict_images = {(23, 45): [[2, 3, 4], [4, 6, 7]],
                             (45, 56): [[4, 5, 6], [7, 8, 9]],
                             (44,  55): [[5, 66, 7], [7, 7, 9]]}
keys = list(dict_images.keys())
for k, v in dict_images.items():
    _, im = k, v
    nearest_x = find_nearest(k[0], k[1], keys)
    print((nearest_x[0], nearest_x[1]))
