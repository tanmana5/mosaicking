"""Code to stitch multiple images."""
from __future__ import print_function
from panorama import Stitcher
import argparse
import imutils
from PIL import Image
import os
import os.path as osp
import cv2


# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
# ap.add_argument('-f', '--first', required=True,
#                 help='path to the first image')
# ap.add_argument('-s', '--second', required=True,
#                 help='path to the second image')
ap.add_argument('-p', '--path', required=True,
                help='path to directory of images')
args = vars(ap.parse_args())

path = args['path']

imgs = []
valid_img_ext = [".jpg", ".jpeg", ".png", ".tiff"]
for f in os.listdir(path):
    ext = osp.splitext(f)[1]
    fname = osp.splitext(f)[0]
    if ext.lower() not in valid_img_ext or 'label' in fname:
        continue
    imgs.append(osp.join(path, f))

master = cv2.imread(imgs[0])
for i, img_path in enumerate(imgs):
    print(master.shape)
    imageA = cv2.imread(img_path)
    # imageA = imutils.resize(imageA, width=400, height=300)
    # stitch the images together to create a panorama
    stitcher = Stitcher()
    (master, vis) = stitcher.stitch([master, imageA], showMatches=True)
    cv2.imshow('Result', master)
    cv2.waitKey(0)
    if i == 2:
        break
cv2.imshow('Result', imutils.resize(master, width=400, height=300))
