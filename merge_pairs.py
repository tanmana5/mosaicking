""" This code merges a given pair of images."""
from __future__ import print_function
from panorama import Stitcher
import numpy as np
import cv2
import imutils
import argparse
from PIL import Image
import os
import os.path as osp


def merge_pair(first, sec):
    first = imutils.resize(first, width=400, height=300)
    sec = imutils.resize(sec, width=400, height=300)
    stitcher = Stitcher()
    result, vis = stitcher.stitch((first, sec), showMatches=True)
    # cv2.imshow('Result', vis)
    print(result, result.shape, result.shape)
    # cv2.imshow("Image A", first)
    # cv2.imshow("Image B", sec)
    # cv2.imshow("Keypoint Matches", result)
    cv2.imshow("Result", result)
    cv2.waitKey(0)
    return result


if __name__ == '__main__':
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument('-f', '--first', required=True,
                    help='path to the first image')
    ap.add_argument('-s', '--second', required=True,
                    help='path to the second image')
    # ap.add_argument('-i', '--images', required=True, action='append',
    #                 help='list of images')
    ap.add_argument('-p', '--path', required=True,
                    help='path to directory of images')
    args = vars(ap.parse_args())
    path = args['path']
    # list_images = [a for a in args['images']]

    # for i, img in enumerate(list_images):
    #     first = list_images[i]
    #     sec = list_images[i+1]
    first = cv2.imread(osp.join(args['path'], args['first']))
    sec = cv2.imread(osp.join(args['path'], args['sec']))
    merge_pair(first, sec)
        #i += 2
